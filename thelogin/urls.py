from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name="thelogin"
urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('register/', views.registerViews, name='register'),
    path('logout/', LogoutView.as_view(next_page='thelogin:login'), name='logout'), 
]
# template_name='registration/login.html'