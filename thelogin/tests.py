from django.test import TestCase, LiveServerTestCase, RequestFactory, Client
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .views import registerViews
from django.urls import resolve

# Create your tests here.

class StoryLogin(TestCase):
    def test_landing(self):
        responseone = self.client.get("/accounts/register/")
        self.assertTemplateUsed(responseone, 'register.html')
        self.assertEqual(responseone.status_code, 200)

    def test_landing_two(self):
        responsetwo = self.client.get("/accounts/login/")
        self.assertTemplateUsed(responsetwo, 'registration/login.html')
        self.assertEqual(responsetwo.status_code, 200)

    def test_landing_views(self):
        response = resolve("/accounts/register/")
        self.assertEqual(response.func, registerViews)