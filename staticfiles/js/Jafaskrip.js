$(document).ready(function(){
    $('#darkmode').click(function(){
        if($('#darkmode').html() == 'Dark Mode'){
            $('#darkmode').html('Light Mode');
        }
        else{
            $('#darkmode').html('Dark Mode');
        }
        $('body').toggleClass('dark');
        $('h1').toggleClass('saya');
        $('.deskripsi').toggleClass('yuhu');
        $('.btn-primary').toggleClass('gelapmode');
        $('.nav-link').toggleClass('yiha');
        $('.accordion__button').toggleClass('darker');
        $('.ui-accordion .ui-accordion-header').toggleClass('berlawanan')
    })
    $(function() {
        $("#accordion").accordion({
            active:false,
            collapsible:true,
            animate:100
        });
    });
})