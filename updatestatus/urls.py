from django.urls import path
from . import views

app_name="updatestatus"
urlpatterns = [
    path('', views.index, name='index'),
    path('onlypage/', views.onlypage, name='onlypage'),
]