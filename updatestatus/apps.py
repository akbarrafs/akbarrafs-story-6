from django.apps import AppConfig


class UpdatestatusConfig(AppConfig):
    name = 'updatestatus'
