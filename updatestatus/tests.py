from django.test import TestCase, LiveServerTestCase
from .models import UpdateStatus
from .views import index, onlypage
from .forms import UpdateForm
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class StoryEnamUnitTest(TestCase):
    def test_landing(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response, 'UpdateYuk.html')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,'<form')
    
    def test_landing_two(self):
        responsing = self.client.get("/onlypage/")
        self.assertTemplateUsed(responsing, 'Jafaskrip.html')
        self.assertEqual(responsing.status_code, 200)

    def test_landing_views(self):
        response = resolve("/")
        self.assertEqual(response.func, index)

    def create_status(self):
        new_status = UpdateStatus.objects.create(status = "Coba Status")
        return new_status

    def test_check_status(self):
        c = self.create_status()
        self.assertTrue(isinstance(c, UpdateStatus))
        self.assertTrue(c.__str__(), c.status)
        counting_all_status = UpdateStatus.objects.all().count()
        self.assertEqual(counting_all_status, 1)
    
    def test_form(self):
        data_form = {
            'status' : 'ini adalah sebuah status',
        }
        form = UpdateForm(data = data_form)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data_form)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


class StoryEnamFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(StoryEnamFunctionalTest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()
        super(StoryEnamFunctionalTest, self).tearDown()

    def test_response(self):
        self.browser.get(self.live_server_url)

        kontain = self.browser.find_element_by_class_name('kontener')
        self.assertNotEqual(kontain.find_element_by_tag_name('h1').text, "Hello, what's on your mind ?")

    def test_input_status(self):
        newstatus = UpdateStatus.objects.create(status = "Aku Pusing")
        self.browser.get(self.live_server_url)
        kontain = self.browser.find_element_by_class_name('kontener')
        self.assertNotEqual(kontain.find_element_by_tag_name('p').text, "Aku Pusing")