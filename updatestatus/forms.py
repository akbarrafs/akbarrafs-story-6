from django import forms
from .models import UpdateStatus
from django.forms import widgets

class UpdateForm(forms.ModelForm):

    class Meta():
        model = UpdateStatus
        fields = {'status'}
        widgets = {
            'status' : forms.Textarea(attrs={'cols':10, 'rows': 5}),
        }

    def __init__(self, *args, **kwargs):
        super(UpdateForm,self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
