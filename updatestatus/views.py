from django.shortcuts import render,redirect
from .models import UpdateStatus
from .forms import UpdateForm
from django.contrib.auth.decorators import login_required


# Create your views here.
# @login_required
def index(request):
    if request.method == "POST":
        form = UpdateForm(request.POST)
        if form.is_valid():
            status = form.save(commit=False)
            status.save()
            return redirect('updatestatus:index')
    else:
        form = UpdateForm()
    status = UpdateStatus.objects.all().order_by('-waktu')
    return render(request, 'UpdateYuk.html', {'form' : form, 'status' : status})

# @login_required
def onlypage(request):
    return render(request,'Jafaskrip.html')
